function notes
  set workpath $repos/crispy-journey
	if not set -q argv[1]:
		cd $workpath 
	else
		if test $argv[1] = '-e'
			set edit true
			set -e argv[1]
		else if test $argv[1] = '-p'
			set path true
			set -e argv[1]
		end
		
		set lfile (find $workpath -type f -wholename ( string join '' "*" $argv[1] "*") ! -wholename "$workpath/.git/*" )
		
		if test (count $lfile) -eq 0
			return 127
		else if test (count $lfile) -eq 1
			if set -q edit
				nvim $lfile
			else if set -q path
				ll $lfile | awk '{print $NF}'
			else 
				cat $lfile
			end
		else
			ll $lfile
		end
	end
end


function loop  --description "loop <count> <command>"
	set count $argv[1]
  set --erase argv[1]
	echo
	echo command: $argv
	echo count: $count
	echo ------------
	
  for i in (seq 1 $count)
    eval $argv
  end
end


function awsregion
	export AWS_DEFAULT_REGION=$argv
end


function importgitkey
	#alias launchsshagent="eval (ssh-agent -s | sed 's/export.*;//g' | sed 's/SSH_AUTH_SOCK=/export SSH_AUTH_SOCK=/g' | sed 's/SSH_AGENT_PID=/export SSH_AGENT_PID=/g')"
	if not set -q SSH_AUTH_SOCK;
		launchsshagent
	end
	if not set -q argv[1] #Check if any arguments are passed to function
		ssh-add $bitbucketkey
	else
		ssh-add $argv
	end
end


function key
    #set credential_dir 
	set lfile (find $credentials_dir -type f -name ( string join '' "*" $argv[1] "*") ! -name "*.pub" ! -wholename "$credentials_dir/.git/*")	
        if test (count $lfile) -eq 0
			return 127
        else if test (count $lfile) -eq 1
            echo $lfile | awk '{print $NF}'
        else 
            ll $lfile
        end
end


function work
	#set workdir 
	set dir (find $workdir -mindepth 2 -maxdepth 2 -type d -name ( string join '' "*" $argv[1] "*") )	
	if test (count $dir) -eq 0
			return 127
	else if test (count $dir) -eq 1
	    cd $dir | awk '{print $NF}'
	else
		for i in $dir; echo $i;end
	end
end
