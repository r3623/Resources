# >>> personal settings >>>

# Required Additional Programs:
# 	- batcat: /sharkdp/bat
# 	- ripgrep: BurntSushi/ripgrep
# 	- exa: ogham/exa
#
# 	Check https://github.com/Alanthiel/crispy-journey/blob/main/linux-notes/packages.yml for information of additional packages used

set fish_greeting

#Custom Envs
export DOCKER_BUILDKIT=1
export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/bin"
export XDG_CONFIG_HOME="$HOME/.config"
export SAM_CLI_TELEMETRY=0
export EDITOR='nvim'


#Configurations:
set BAT_CONFIG_PATH "$XDG_CONFIG_HOME/bat/config"
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"

# Directories setup
set workdir "$HOME/workbench"
set repos "$HOME/repos"
set logloc $workdir"System/Logging and Rollback"
set localbin "$HOME/.local/bin"
set internal_resources "$HOME/internal-resources"
set credentials_dir "$internal_resources/credentials"

# Quick Config Variables
set config "$XDG_CONFIG_HOME/fish/conf.d/var.fish"
set interface "$XDG_CONFIG_HOME/fish/conf.d/interface.fish"

# Command Replacements / Flag setups
alias ping="ping -c 5 "

alias grep="rg --color=always"
alias cat="bat"

alias tree="exa --tree --icons"
alias ls="exa --sort=type"
alias la="exa -laa --git --sort=type"
alias ll="exa -l --git --sort=type"
alias vim="nvim"

# Aliases Commands
alias jloc='curl https://json.geoiplookup.io/(curl https://ipinfo.io/ip) | jq'
alias emptyfolder="rm -r ./*"
alias shredcontents="find . -type f -exec shred -ufz {} \; && rm -rf * 2> /dev/null"
alias cettime="TZ=CET date"
alias utctime='TZ=UTC date'
alias launchsshagent="eval (ssh-agent -s | sed 's/export.*;//g' | sed 's/SSH_AUTH_SOCK=/export SSH_AUTH_SOCK=/g' | sed 's/SSH_AGENT_PID=/export SSH_AGENT_PID=/g')"
alias reconf="source $config"
alias killssh="killall ssh-agent"
alias secure-docker-run='docker run -it --rm --cap-drop=all --security-opt=no-new-privileges:true --memory 128M --cpus 0.75 --tmpfs /tmp:rw,noexec,nosuid,size=64k'


# Init Commands

starship init fish | source

