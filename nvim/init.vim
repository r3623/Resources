set guicursor=n-i-c:hor30-iCursor-blinkwait100-blinkon50-blinkoff50
set guicursor+=v:ver100-iCursor-blinkwait100-blinkon50-blinkoff50

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
command Nerdtree NERDTree

command Spellcheck set spell spelllang=en_us
command Indentfold set foldmethod=indent

let g:NERDTreeWinPos = "right"

augroup python
        autocmd!
	auto FileType py,pyc let python_highlight_builtins = 1 |
				\ let python_highlight_exceptions = 1 |
				\ let python_highlight_string_formatting = 1 |
				\ let python_highlight_string_format = 1 |
				\ let python_highlight_indent_errors = 1 |
				\ let python_highlight_func_calls = 1 |
				\ let python_highlight_class_vars = 1 |
				\ let python_highlight_operators = 1
				\ let python_highlight_file_headers_as_comments = 1 
augroup end

call plug#begin()
Plug 'frazrepo/vim-rainbow'
Plug 'Yggdroot/indentLine'
Plug 'preservim/nerdtree' |
	\ Plug 'Xuyuanp/nerdtree-git-plugin' |
	\ Plug 'ryanoasis/vim-devicons'
Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }
Plug 'vim-python/python-syntax'
Plug 'mtdl9/vim-log-highlighting'
Plug 'ekalinin/Dockerfile.vim'
Plug 'dag/vim-fish'
Plug 'wfxr/minimap.vim'
call plug#end()
